## full_oppo8786_wlan-user 11 RP1A.200720.011 378 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMP2103
- Brand: realme
- Flavor: full_oppo8786_wlan-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1645002403921
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMP2103EEA/RE54C1L1:11/RP1A.200720.011/1645002403921:user/release-keys
realme/RMP2103RU/RE54C1L1:11/RP1A.200720.011/1645002403921:user/release-keys
realme/RMP2103/RE54C1L1:11/RP1A.200720.011/1645002403921:user/release-keys
realme/RMP2103TR/RE54C1L1:11/RP1A.200720.011/1645002403921:user/release-keys
- OTA version: 
- Branch: full_oppo8786_wlan-user-11-RP1A.200720.011-378-release-keys
- Repo: realme_rmp2103_dump_8756


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
